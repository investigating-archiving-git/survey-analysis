# IASGE Survey Data Analysis

[![forthebadge](https://forthebadge.com/images/badges/built-with-grammas-recipe.svg)](https://forthebadge.com) [![forthebadge](https://forthebadge.com/images/badges/cc-sa.svg)](https://forthebadge.com)

## About
This book focuses on preparing and analyzing survey data downloaded from Qualtrics for analysis, as a part of the [Investigating & Archiving the Scholarly Git Experience](https://investigating-archiving-git.gitlab.io/) project. We are providing all the data and code openly to facilitate reuse, reproducibility, and extension of our work.

## Build our book locally
1. Fork, clone or download this project (See #1-4 in the [contributing guide](CONTRIBUTING.md))
2. Install R & RStudio
3. Install the bookdown, RMarkdown, and tinytex packages in RStudio with the following two commands in the R terminal:
	* `install.packages(c("rmarkdown", "bookdownplus", "tinytex", "webshot", "tidyverse", "here", "ggplot2", "knitr", "kableExtra", "tidytext", "ggpubr", "stringr"))`
  	* `tinytex::install_tinytex()`
	* `webshot::install_phantomjs()`
	
	You can also click Tools > Install Packages and type the package names (make sure "install dependencies" is checked) separated by commas.
	
4. Go to the project folder and double click `survey-analysis.Rproj` to start RStudio
5. Run this command in the R Console after RStudio opens: `bookdown::render_book('index.Rmd', 'all')`
6. Go to the folder `_book` in the project folder and click `index.html` to view the book locally in your browser.

## Contact info
You are welcome to email me at [vicky dot rampin at nyu dot edu](mailto:vicky.rampin@nyu.edu) if you have questions or concerns, or raise an issue on this repository and I will do my best to respond quickly!
